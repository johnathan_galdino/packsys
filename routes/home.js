var express = require('express');
var router = express.Router();


router.get('/', function (req, res, next) {
    res.render('home', { titlebar : "Meus modulos",
        title: 'Express', whichPartial: () => {

            return "myaaps"
        }
    });
});

router.get('/:id', function (req, res, next) {
    res.render('home', { titlebar : "Meus modulos",
        title: 'Express', whichPartial: () => {

            return req.params.id;
        }
    });
});

router.get('/apps/:id', function (req, res, next) {

    console.log(req.params.id);

    res.render('home', {
        title: 'Express', id: req.params.id, whichPartial: () => {

            return "ap";
        }
    });
});

module.exports = router;